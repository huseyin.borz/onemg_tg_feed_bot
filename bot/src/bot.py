import logging

import conf.settings as settings

from telegram.ext import Updater

from handler import CommandHandler, CallbackHandler, FilteredMessageHandler

logger = logging.getLogger(__name__)


class Bot(object):
    def __init__(self, token=None, **kwargs):
        self.token = token

    def run(self):
        logger.info('Bot is started...')
        updater = Updater(self.token, request_kwargs=settings.TG_REQUEST_KWARGS)
        dp = updater.dispatcher

        dp.add_handler(CommandHandler())
        # dp.add_handler(CallbackHandler())
        dp.add_handler(FilteredMessageHandler())
        # dp.add_error_handler(error_handler)

        if settings.UPDATES_MODE == 'polling':
            updater.start_polling()
        elif settings.UPDATES_MODE == 'webhook':
            key = open(settings.SSL_KEYF, 'rb') if settings.SSL_KEYF is not None else None
            cert = open(settings.SSL_CERTF, 'rb') if settings.SSL_CERTF is not None else None

            updater.start_webhook(
                listen=settings.WEBHOOK_LISTEN_HOST,
                port=settings.WEBHOOK_LISTEN_PORT,
                url_path=settings.TG_TOKEN,
                key=key,
                cert=cert,
                webhook_url=settings.WEBHOOK_URL
            )

        updater.idle()
