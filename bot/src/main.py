from bot import Bot
from conf.settings import TG_TOKEN


if __name__ == '__main__':
    Bot(token=TG_TOKEN).run()
