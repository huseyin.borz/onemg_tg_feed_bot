from sqlalchemy.sql import text
from typing import List

from conf.db import db
from domain.entities import Channel


class SubscriptionRepository:
    def has(self, user_id: int, channel_id: int) -> bool:
        """
        Check if such subscription relation exists
        :param user_id: ID of user
        :param channel_id: ID of channel
        :return: Is subscription exists
        """
        return db.session \
            .execute(
                text("""SELECT COUNT(*) FROM subscriptions WHERE user_id = :user_id AND channel_id = :channel_id"""),
                {'user_id': user_id, 'channel_id': channel_id}
            ) \
            .fetchone()[0] > 0

    def create(self, user_id: int, channel_id: int, tags: List[str]) -> bool:
        """
        Create subscription relation
        :param user_id: ID of user
        :param channel_id: ID of channel
        :param tags: List of tags for filter
        :throws sqlalchemy.exc.InvalidRequestError if subscription already exists
        :rtype: bool
        :return: Create successful or not
        """
        return db.session \
            .execute(
                text("""INSERT INTO subscriptions (user_id, channel_id, tags) VALUES (:user_id, :channel_id, :tags)"""),
                {'user_id': user_id, 'channel_id': channel_id, 'tags': tags}
            ) \
            .rowcount > 0

    def remove(self, user_id: int, channel_id: int) -> int:
        """
        Remove subscription relation
        :param user_id: ID of user
        :param channel_id: ID of channel
        :rtype: int
        :return How many left subscribers to this channel
        """
        return db.session \
            .execute(
                text(
                    """
                    DELETE FROM subscriptions WHERE user_id = :user_id AND channel_id = :channel_id;
                    SELECT COUNT(*) FROM subscriptions WHERE channel_id = :channel_id;
                    """
                ),
                {'user_id': user_id, 'channel_id': channel_id}
            ) \
            .fetchone()[0]

    def list(self, user_id: int) -> List[Channel]:
        """
        Get list of channels user subscribed to
        :param user_id: ID of user
        :rtype: List[Channel]
        :return: List of channels
        """
        return db.session \
            .execute(
                text(
                    """
                    SELECT ch.name, ch.url, sub.tags
                    FROM Subscriptions AS sub
                    JOIN Channels AS ch ON ch.id = sub.channel_id
                    WHERE sub.user_id = :user_id
                    """
                ),
                {'user_id': user_id}
            ) \
            .fetchall()

    def add_tag(self, user_id: int, channel_id: int, tags: List[str]) -> Channel:
        """
        Update subscription by adding tags
        :param user_id: ID of user
        :rtype: Channel
        :return: Channel
        """
        return db.session \
            .execute(
                text(
                    """
                    UPDATE subscriptions sub SET tags=tags || CAST(:tags AS varchar[])
                    WHERE sub.user_id = :user_id AND sub.channel_id = :channel_id;
                    SELECT ch.* FROM channels ch WHERE ch.id = :channel_id;
                    """
                ),
                {'user_id': user_id, 'channel_id': channel_id, 'tags': tags}
            ) \
            .fetchone()

    def remove_tag(self, user_id: int, channel_id: int, tags: List[str]) -> Channel:
        """
        Remove tag from subscription
        :param user_id: ID of user
        :rtype: Channel
        :return: Channel
        """
        return db.session \
            .execute(
                text(
                    """
                    UPDATE subscriptions sub SET tags=array_remove_array(sub.tags, CAST(:tags AS varchar[]))
                    WHERE sub.user_id = :user_id AND sub.channel_id = :channel_id;
                    SELECT ch.* FROM channels ch WHERE ch.id = :channel_id;
                    """
                ),
                {'user_id': user_id, 'channel_id': channel_id, 'tags': tags}
            ) \
            .fetchone()

    def get_tags(self, chat_url: str) -> List[Channel]:
        """
        Get tags with redirect_url for specific chat_id
        :param chat_id: ID of chat
        :rtype: List[Channel]
        :return: List of channels
        """
        return db.session \
            .execute(
                text(
                    """
                    SELECT u.redirect_url, sub.tags FROM subscriptions sub
                    JOIN users u ON u.id = sub.user_id
                    JOIN channels ch ON ch.url = :chat_url
                    WHERE u.redirect_url IS NOT NULL AND sub.channel_id = ch.id
                    """
                ),
                {'chat_url': chat_url}
            ) \
            .fetchall()
