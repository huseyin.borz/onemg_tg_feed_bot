import logging

from conf.db import db
from repository import ChannelRepository, SubscriptionRepository, UserRepository
from domain.command import Command
from exception.settings_exception import RedirectChangeError, RedirectNotAllowed, GenericSettingsError, \
    RedirectChangeError

logger = logging.getLogger(__name__)


class Settings:
    """
    User settings management
    """
    def __init__(self):
        pass

    def add_redirect(self, command: Command):
        def callback():
            user = UserRepository().get_or_create(telegram_id=command.chat_id)

            channel = ChannelRepository().get(command.channel_url)
            if channel:
                has_sub = SubscriptionRepository().has(user_id=user.id, channel_id=channel.id)
                if has_sub:
                    raise RedirectNotAllowed()

            UserRepository().change_settings(telegram_id=user.telegram_id, redirect_url=command.channel_url)

        try:
            db.execute_in_transaction(callback)
        except GenericSettingsError:
            raise
        except:
            logger.exception("Failed to add redirect")
            raise RedirectChangeError("Failed to add redirect")

    def remove_redirect(self, command: Command):
        def callback():
            user = UserRepository().get(telegram_id=command.chat_id)
            if user is None or user.redirect_url is None:
                raise RedirectChangeError("You haven't added a redirect!")

            UserRepository().change_settings(telegram_id=user.telegram_id, redirect_url=None)

        try:
            db.execute_in_transaction(callback)
        except GenericSettingsError:
            raise
        except:
            logger.exception("Failed to remove redirect")
            raise RedirectChangeError("Failed to remove redirect")
