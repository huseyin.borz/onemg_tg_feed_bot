import logging

from typing import List

from sqlalchemy.exc import IntegrityError

from telegram import Message

from conf.db import db
from repository import ChannelRepository, SubscriptionRepository, UserRepository
from domain.command import Command
from domain.entities import Channel, Subscription
from exception.subscription_exception import *

logger = logging.getLogger(__name__)


class Subscriptions:
    """
    Subscriptions management
    """
    def __init__(self):
        pass

    def subscribe(self, command: Command) -> Channel:
        """
        Handle subscription request
        :param command: Command entity
        :return Channel data
        """
        if command.channel_url is None:
            raise IllegalChannelUrlError()

        def callback():
            user = UserRepository().get_or_create(command.chat_id)
            channel = ChannelRepository().get_or_create(url=command.channel_url, name=command.channel_name)

            if command.tags is None:
                raise EmptyTagListError()

            SubscriptionRepository().create(user_id=user.id, channel_id=channel.id, tags=command.tags)

            return channel

        try:
            return db.execute_in_transaction(callback)
        except EmptyTagListError as e:
            raise
        except IntegrityError:
            raise AlreadySubscribedError()
        except Exception as e:
            logger.error(f"Failed to subscribe: {e}")
            raise SubscribeError()

    def unsubscribe(self, command: Command) -> Channel:
        """
        Handle unsubscription request
        :param command: Command entity
        :return Channel data
        """
        if command.channel_url is None:
            raise IllegalChannelUrlError()

        def callback():
            user = UserRepository().get(command.chat_id)
            if user is None:
                raise NotSubscribedError()

            channel = ChannelRepository().get(command.channel_url)
            if channel is None:
                raise NotSubscribedError()

            has_sub = SubscriptionRepository().has(user_id=user.id, channel_id=channel.id)
            if not has_sub:
                raise NotSubscribedError()

            subs_left = SubscriptionRepository().remove(user_id=user.id, channel_id=channel.id)
            if subs_left == 0:
                preserved_channel = Channel()
                preserved_channel.name = channel.name
                preserved_channel.url = channel.url
                channel = preserved_channel
                ChannelRepository().remove(command.channel_url)

            return channel

        try:
            return db.execute_in_transaction(callback)
        except UnsubscribeError:
            raise
        except Exception as e:
            logger.error(f"Failed to unsubscribe: {e}")
            raise UnsubscribeError()

    def list(self, command: Command) -> List[Channel]:
        """
        Handle channels list request
        :param command: Command entity
        :return: List of channels user subscribed to
        """
        try:
            user = UserRepository().get(command.chat_id)
            if user is None:
                return []

            return SubscriptionRepository().list(user.id)
        except:
            logger.exception("Failed to list subscriptions")
            raise SubscriptionsListError()

    def add_tag(self, command: Command) -> Channel:
        """
        Handle subscription tags
        :param command: Command entity
        :return: Channel data
        """
        if command.channel_url is None:
            raise IllegalChannelUrlError()

        def callback():
            user = UserRepository().get(command.chat_id)
            if user is None:
                raise NotSubscribedError()

            channel = ChannelRepository().get(command.channel_url)
            if channel is None:
                raise NotSubscribedError()
            
            has_sub = SubscriptionRepository().has(user.id, channel.id)
            if has_sub is None:
                raise NotSubscribedError()

            if command.tags is None:
                raise EmptyTagListError()

            channel = SubscriptionRepository().add_tag(user.id, channel.id, command.tags)
            return channel

        try:
            return db.execute_in_transaction(callback)
        except EmptyTagListError as e:
            raise
        except Exception as e:
            logger.error(f"Failed to add tags: {e}")
            raise GenericSubscriptionError(str(e))

    def remove_tag(self, command: Command) -> Channel:
        """
        Remove subscription tags
        :param command: Command entity
        :return: Channel data
        """
        if command.channel_url is None:
            raise IllegalChannelUrlError()
        
        def callback():
            user = UserRepository().get(command.chat_id)
            if user is None:
                raise NotSubscribedError()

            channel = ChannelRepository().get(command.channel_url)
            if channel is None:
                raise NotSubscribedError()
            
            has_sub = SubscriptionRepository().has(user.id, channel.id)
            if has_sub is None:
                raise NotSubscribedError()

            if command.tags is None:
                raise EmptyTagListError()

            channel = SubscriptionRepository().remove_tag(user.id, channel.id, command.tags)
            return channel

        try:
            return db.execute_in_transaction(callback)
        except EmptyTagListError as e:
            raise
        except Exception as e:
            logger.error(f"Failed to add tags: {e}")
            raise GenericSubscriptionError(str(e))

    def get_tags(self, message: Message) -> List[Channel]:
        """
        Get list of tags and redirect urls
        :param message: Telegram Message entity
        :return: Channel data
        """
        try:
            return SubscriptionRepository().get_tags(message.chat.username)
        except:
            logger.exception("Failed to get distincted list of tags")
            raise GenericSubscriptionError()
