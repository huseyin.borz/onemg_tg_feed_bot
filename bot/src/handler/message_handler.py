import re
import logging

from typing import List

from domain.channel_post import ChannelPost
from service.subscriptions_service import Subscriptions
from utils.channel_post_producer import ChannelPostProducer
from utils.custom_filter import message_filter

from telegram import Update, Message
from telegram.ext import MessageHandler, Filters

from telegram.ext.dispatcher import run_async
from retry import retry

logger = logging.getLogger(__name__)


class FilteredMessageHandler(MessageHandler):
    filters: Filters = message_filter
    optional_args: dict = {
        'message_updates': False,
        'channel_post_updates': True,
        # Temporary doesn't receive edited message updates
        'edited_updates': False,
        'pass_user_data': True
    }

    def __init__(self):
        super(FilteredMessageHandler, self).__init__(self.filters, self.handle, **self.optional_args)

    def handle_update(self, update, dispatcher):
        optional_args = self.collect_optional_args(dispatcher, update)
        return self.callback(dispatcher.bot, update, **optional_args)

    def __get_chat_ids(self, message: Message) -> List[str]:
        found = False
        redirect_urls = set()

        subscriptions = Subscriptions().get_tags(message)
        for sub in subscriptions:
            text = message.text if message.text else message.caption
            found = any(re.search(tag, text, re.IGNORECASE) for tag in sub.tags)
            if found:
                redirect_urls.add(sub.redirect_url)
                found = False

        return list(redirect_urls)

    @run_async
    @retry(tries=5, delay=1)
    def handle(self, bot, update, user_data=None, **kwargs):
        chat_ids = self.__get_chat_ids(update.channel_post)
        new_channel_post = ChannelPost(chat_ids, update)

        logger.info(f'Produce message: {new_channel_post.to_dict()}')
        ChannelPostProducer().produce(msg=new_channel_post.to_dict())
