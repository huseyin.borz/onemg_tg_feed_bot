import logging

from service.user_settings_service import Settings
from exception.settings_exception import GenericSettingsError, BotNotAddedAsAdminError
from .base_command import BaseCommand

from telegram.error import Unauthorized

logger = logging.getLogger(__name__)


class Redirect(BaseCommand):
    name = 'redirect'
    aliases = ['r']

    def execute(self, command):
        if not command.is_private():
            self.reply(command, 'Groups currently are not supported!')

        try:
            if command.channel_url is None:
                Settings().remove_redirect(command)
                self.reply(command, "Successfully removed redirect")
            else:
                self.__try_post_message(command)
                Settings().add_redirect(command)
                self.reply(command, f"Successfully added redirect to @{command.channel_url})")
        except GenericSettingsError as e:
            self.reply(command, str(e))

    def __try_post_message(self, command):
        try:
            logger.debug(f"Trying to post message to channel...")
            self.bot.send_message(chat_id=f"@{command.channel_url}", text="Hello there :)")
        except Unauthorized:
            raise BotNotAddedAsAdminError()
