from typing import List

from telegram.parsemode import ParseMode

from service.subscriptions_service import Subscriptions
from domain.entities import Channel
from exception.subscription_exception import GenericSubscriptionError
from .base_command import BaseCommand


class List(BaseCommand):
    name = 'list'
    aliases = ['l']

    def execute(self, command):
        if not command.is_private():
            self.reply(command, 'Groups currently are not supported!')

        try:
            channels = Subscriptions().list(command)
            self.reply(command, self.__format_to_string(channels), parse_mode=ParseMode.MARKDOWN)
        except GenericSubscriptionError as e:
            self.reply(command, str(e))

    @staticmethod
    def __format_to_string(channels: List[Channel]) -> str:
        if len(channels) == 0:
            return "You don't have active subscriptions."

        channels.sort(key=lambda c: c.name)

        result = []
        counter = 1
        for ch in channels:
            formatted_string = f''
            for tag in ch.tags:
                formatted_string += f'#{tag} '
            result.append(
                f"{counter}. *{ch.name}* ([@{ch.url}](https://t.me/{ch.url})): **{formatted_string}**"
            )
            counter += 1

        return "\n".join(result)
