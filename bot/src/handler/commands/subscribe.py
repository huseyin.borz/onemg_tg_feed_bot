from service.subscriptions_service import Subscriptions
from exception.subscription_exception import GenericSubscriptionError
from .base_command import BaseCommand


class Subscribe(BaseCommand):
    name = 'subscribe'
    aliases = ['s']

    def execute(self, command):
        if not command.is_private():
            self.reply(command, 'Groups currently are not supported!')

        try:
            channel = Subscriptions().subscribe(command)
            self.reply(command, f"Successfully subscribed to {channel.name} (@{channel.url})")
        except GenericSubscriptionError as e:
            self.reply(command, str(e))
