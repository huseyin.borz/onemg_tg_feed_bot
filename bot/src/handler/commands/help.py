import os

import conf.settings as settings

from telegram.parsemode import ParseMode
from .base_command import BaseCommand
from utils.utils import read_from_file

INFO_FILE = os.path.join(os.path.dirname(settings.BASE_DIR), os.path.join('resources', 'info', 'help.md'))


class Help(BaseCommand):
    name = 'help'
    text = read_from_file(INFO_FILE)

    def execute(self, command):
        self.reply(command, self.text, parse_mode=ParseMode.MARKDOWN)
