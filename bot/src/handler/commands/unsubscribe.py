from service.subscriptions_service import Subscriptions
from exception.subscription_exception import GenericSubscriptionError
from .base_command import BaseCommand


class Unsubscribe(BaseCommand):
    name = 'unsubscribe'
    aliases = ['u']

    def execute(self, command):
        if not command.is_private():
            self.reply(command, 'Groups currently are not supported!')

        try:
            channel = Subscriptions().unsubscribe(command)
            self.reply(command, f"Successfully unsubscribed from {channel.name} (@{channel.url})")
        except GenericSubscriptionError as e:
            self.reply(command, str(e))
