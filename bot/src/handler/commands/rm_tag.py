from service.subscriptions_service import Subscriptions
from exception.subscription_exception import GenericSubscriptionError
from .base_command import BaseCommand


class RemoveTag(BaseCommand):
    name = 'rmtag'
    aliases = ['rt']

    def execute(self, command):
        if not command.is_private():
            self.reply(command, 'Groups currently are not supported!')

        try:
            channel = Subscriptions().remove_tag(command)
            self.reply(command, f"Successfully remove tags from {channel.name} (@{channel.url})")
        except GenericSubscriptionError as e:
            self.reply(command, str(e))
