from telegram.parsemode import ParseMode
from .base_command import BaseCommand


class Start(BaseCommand):
    name = 'start'
    text = "**Salam guys from onemg.** Have a questions try to use a */help* command."

    def execute(self, command):
        self.reply(command, self.text, ParseMode.MARKDOWN)
