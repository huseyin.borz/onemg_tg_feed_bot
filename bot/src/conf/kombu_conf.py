import ujson
import kombu.serialization


def register_kombu_serializator():
    kombu.serialization.register(
        'ujson', ujson.dumps, ujson.loads,
        content_type='application/json'
    )
