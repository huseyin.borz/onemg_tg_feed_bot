"""array_remove_array

Revision ID: 7d491c7014f6
Revises: 4307e519d12b
Create Date: 2019-01-06 18:09:23.575498

"""
from alembic import op
from sqlalchemy.sql import text


# revision identifiers, used by Alembic.
revision = '7d491c7014f6'
down_revision = '4307e519d12b'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute(
        text(
            """
            CREATE OR REPLACE FUNCTION array_remove_array(arr anyarray, other_arr anyarray)
            RETURNS anyarray AS $$
            DECLARE
                outer_arr varchar[];
                item varchar;
            BEGIN
                IF arr IS NULL or other_arr IS NULL or
                    (array_length(other_arr, 1) > array_length(arr, 1)) THEN
                RETURN arr;
                END IF;

                outer_arr := arr;
                FOREACH item IN ARRAY other_arr LOOP
                    outer_arr := array_remove(outer_arr, item);
                END LOOP;
                RETURN outer_arr;
            END;
            $$ LANGUAGE plpgsql IMMUTABLE;
            """
        )
    )


def downgrade():
    pass
