from typing import List
from dataclasses import dataclass, asdict


@dataclass
class ChannelPost(object):
    chat_ids: List[str]
    from_chat_id: str
    message_id: int

    def __init__(self, chat_ids, update):
        self.chat_ids = chat_ids
        self.from_chat_id = update.channel_post.chat_id
        self.message_id = update.channel_post.message_id

    def to_dict(self):
        return asdict(self)
