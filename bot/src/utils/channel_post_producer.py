import conf.settings as settings

from .base_producer import BaseProducer


class ChannelPostProducer(BaseProducer):
    connection_str = settings.AMQP_URL
    queue_name = settings.CHANNEL_POST_QUEUE
