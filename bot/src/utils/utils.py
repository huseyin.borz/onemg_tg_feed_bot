def read_from_file(file_path: str):
    with open(file_path, 'r') as f:
        data = f.read()
    return data
