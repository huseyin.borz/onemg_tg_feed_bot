from kombu import Connection
from kombu.pools import connections
from kombu.log import get_logger

logger = get_logger(__name__)


__all__ = ['BaseProducer',]


class BaseProducer(object):
    connection_str: str = None
    queue_name: str = None

    def produce(self, msg, **kwargs):
        connection = Connection(self.connection_str)
        # get connection pool
        with connections[connection].acquire(block=True) as conn:
            queue = None
            try:
                queue = conn.SimpleQueue(self.queue_name)
                queue.put(msg, serializer='ujson', **kwargs)
            except Exception as e:
                logger.exception(str(e))
            finally:
                queue.close()
