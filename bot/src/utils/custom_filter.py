import re

from service.subscriptions_service import Subscriptions
from telegram.ext import BaseFilter


class FilterMessages(BaseFilter):
    def filter(self, message):
        found = False
        for sub in Subscriptions().get_tags(message):
            text = message.text if message.text else message.caption
            found = any(re.search(tag, text, re.IGNORECASE) for tag in sub.tags)
            if found:
                break
        return found


message_filter = FilterMessages()
