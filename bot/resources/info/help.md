*List of allowed commands:*
`/subscribe @channel_name #tag1#tag2#tag3` - subscribe to the specific channel and filter channel content by the tags sepparated with the # (hash) symbol
`/unsubscribe @channel_name` – stop receiving new posts from this channel
`/list` – show list of channels
`/help` – get help
`/redirect @your_new_channel`
`/addtag @channel_name #tag1#tag2#tag3` - add list of tags to specific subscription
`/rmtag @channel_name #tag1#tag2#tag3` - remove tags from subscription

You can also use an appropriate alias `/%first_letter%` (e.g., `/s` instead of `/subscribe`)

*Redirect*
If you don't want to receive messages from me and would prefer having a silent channel, try this feature.
1. Create a new, public channel.
2. Add me to this channel's admins and give me permissions to post messages.
3. Send me something like: `/redirect @your_new_channel`

*Feedback*
Contact me for more info guseyin.gasimov@gmail.com
