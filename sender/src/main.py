from sender import Sender
from conf.settings import TG_TOKEN


if __name__ == '__main__':
    Sender(token=TG_TOKEN).run()
