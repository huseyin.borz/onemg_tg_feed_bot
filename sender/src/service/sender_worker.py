import logging

from utils.base_worker import BaseWorker

from telegram import TelegramError

logger = logging.getLogger(__name__)

__all__ = ['SenderWorker',]


class SenderWorker(BaseWorker):
    capacity = 5

    def __init__(self, connection, queue_name, bot):
        super(SenderWorker, self).__init__(connection, queue_name)
        self.bot = bot

    def on_consume_end(self, messages):
        logger.info(f'Sender worker receive messages: {messages}')

        for msg in messages:
            chat_ids = msg['chat_ids']
            from_chat_id = msg['from_chat_id']
            message_id = msg['message_id']

            for chat_id in chat_ids:
                str_chat_id = f'@{chat_id}'
                logger.debug(f'Forward {msg} to {str_chat_id}')
                try:
                    self.bot.forward_message(str_chat_id, from_chat_id, message_id)
                except TelegramError as e:
                    logger.error(str(e))
