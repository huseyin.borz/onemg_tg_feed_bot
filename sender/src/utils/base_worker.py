import socket

from queue import Empty
from time import sleep
from threading import Thread

from kombu.log import get_logger
from kombu.utils.limits import TokenBucket

logger = get_logger(__name__)

__all__ = ['BaseWorker']


class BaseWorker(object):
    """
    The base class for implementing kombu consumer which is get chunk
    of data directly from queue and do some work on received data.

    For did some work after data has been received,
    class should implement `on_consume_end` meth.

    The basic class would need a :attr:`connection` attribute
    which must be a :class:`~kombu.Connection` instance, and
    a :attr: `queue_name` attribute which must be a :class: `~str`.

    Example:
        .. code-block:: python

            class PrintWorker(Worker):
                def on_consume_end(self, messages):
                    for msg in messages:
                        print(f'Received {msg}')

            with kombu.Connection(amqp_url) as conn:
                queue_name = 'default'
                my_worker = PrintWorker(conn, queue_name).run()


    """
    should_stop: bool = False
    capacity: int = 1000
    # buffer for hold received chunks of messages
    messages: list = []

    def __init__(self, connection, queue_name):
        self.conn = connection
        self.queue = connection.SimpleQueue(queue_name)

    def run(self, tokens=1, **kwargs):
        restart_limit = self.restart_limit()
        errors = (self.conn.connection.connection_errors +
                  self.conn.connection.channel_errors)
        while not self.should_stop:
            try:
                if restart_limit.can_consume(tokens):
                    self.consume(limit=self.capacity, **kwargs)
                else:
                    sleep(restart_limit.expected_time(tokens))
            except errors:
                logger.warning(str(errors))

    def instance(self):
        """
        Start instance of worker in separate thread
        """
        thread = Thread(target=self.run)
        thread.start()

    def consume(self, limit=None, timeout=None, safety_interval=1, **kwargs):
        elapsed = 0
        self.messages = []
        for i in range(limit):
            if self.should_stop:
                break
            if not self.on_iteration():
                break
            try:
                self.conn.drain_events(timeout=safety_interval)
            except socket.timeout:
                self.conn.heartbeat_check()
                elapsed += safety_interval
                if timeout and elapsed >= timeout:
                    raise
            except socket.error:
                if not self.should_stop:
                    raise
            else:
                elapsed = 0
        if self.messages:
            return self.on_consume_end(self.messages)

    def restart_limit(self):
        """
        Token Bucket Algorithm.
        The algorithm consists of a bucket with a maximum capacity of N tokens
        which refills at a rate R tokens per second. Each token typically
        represents a quantity of whatever resource is being
        rate limited (network bandwidth, connections, etc.).

        This allows for a fixed rate of flow R with bursts
        up to N without impacting the consumer.

        See Also:
            https://en.wikipedia.org/wiki/Token_Bucket
        """
        return TokenBucket(self.capacity)

    def on_iteration(self):
        """
        Handler called for every iteration while draining events.
        """
        try:
            msg = self.queue.get(block=True, timeout=1)
            self.messages.insert(0, msg.payload)
            msg.ack()
        except Empty:
            return False
        except Exception as e:
            logger.exception(str(e))
            return False
        else:
            return True

    def on_consume_end(self, messages):
        """
        Handler called after the consumer receive num of
        messages = `capacity` attribute in base class.

        :param messages:
        buffer for hold just received messages.
        """
        raise NotImplementedError('Method is not implemented in base class.')
