import os

from envparse import env
from logging.config import dictConfig

from conf.kombu_conf import register_kombu_serializator

# Register ujson as kombu serializator
register_kombu_serializator()

# Read env file
env.read_envfile()


LOG_LEVEL = env('LOG_LEVEL', 'ERROR')

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Telegram bot configuration

TG_TOKEN = env('TG_TOKEN')
TG_PROXY_URL = env('TG_PROXY_URL')
TG_PROXY_USER = env('TG_PROXY_USER')
TG_PROXY_PASS = env('TG_PROXY_PASS')
TG_REQUEST_KWARGS = {
    'proxy_url': TG_PROXY_URL,
    # Optional, if you need authentication:
    'urllib3_proxy_kwargs': {
        'username': TG_PROXY_USER,
        'password': TG_PROXY_PASS,
    }
}


# AMQP configuration

AMQP_URL = env('AMQP_URL', 'amqp://')

SENDER_WORKER_QUEUE = env('SENDER_WORKER_QUEUE', 'channel_posts')


# Setup logging

LOG_DICT = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'default': {
            'format': '[%(asctime)s][%(levelname)s] %(name)s '
                      '%(filename)s:%(funcName)s:%(lineno)d || %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
    },
    'loggers': {
        'main': {
            'handlers': ('console',),
            'level': LOG_LEVEL,
        },
        'conf': {
            'handlers': ('console',),
            'level': LOG_LEVEL,
        },
        'service': {
            'handlers': ('console',),
            'level': LOG_LEVEL,
        },
        'sender': {
            'handlers': ('console',),
            'level': LOG_LEVEL,
        },
        'root': {
            'handlers': ('console',),
            'level': LOG_LEVEL,
        }
    },
}

dictConfig(LOG_DICT)
