import logging

import conf.settings as settings
from service.sender_worker import SenderWorker

from kombu import Connection
from telegram.ext import Updater

logger = logging.getLogger(__name__)

__all__ = ['Sender',]


class Sender(object):
    def __init__(self, token=None):
        self.token = token

    def run(self):
        logger.info('Sender is started...')
        updater = Updater(self.token, request_kwargs=settings.TG_REQUEST_KWARGS)
        conn = Connection(settings.AMQP_URL)
        SenderWorker(conn, settings.SENDER_WORKER_QUEUE, updater.bot).instance()
        updater.idle()
