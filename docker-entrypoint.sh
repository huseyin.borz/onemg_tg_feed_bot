#!/bin/bash

: ${ENV_SECRETS_DIR:=/run/secrets}


for env_var in $(printenv | cut -f1 -d"=")
do
    var="$env_var"
    eval val=\$$var
    if secret_name=$(expr match "$val" "##SECRET:\([^}]\+\)##$"); then
        secret="${ENV_SECRETS_DIR}/${secret_name}"

        echo -e "Secret file for $var: $secret"

        if [ -f "$secret" ]; then
            val=$(cat "${secret}")
            export "$var"="$val"
            echo "Expanded variable: $var"
        else
            echo "Secret file does not exist! $secret"
        fi
    fi
done

export "DATABASE_URL=postgres://$DB_USER:$DB_PASS@$DB_HOST:$DB_PORT/$DB_NAME"
export "AMQP_URL=amqp://$RABBITMQ_LOGIN:$RABBITMQ_PASSWORD@$RABBITMQ_HOST:$RABBITMQ_PORT/$RABBITMQ_VHOST"

exec "$@"
